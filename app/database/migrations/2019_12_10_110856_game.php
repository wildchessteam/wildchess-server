<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Game extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('games', function (Blueprint $table){
			// id партии (поле автоинкремент)
			$table -> bigIncrements('id');
			//  этот метод создаст поля created_at и updated_at
			$table -> timestamps();
			$table -> string('name')
				->charset('utf8')
				->comment("название партии")
			;
			$table -> tinyInteger('author_id')
				->comment("Id автора")
			;

			$table -> tinyInteger('status_id')
				->comment("Id статуса партии")
			;
            
            $table ->boolean("viewers")
                ->default(true)
				->comment("Позволить наблюдать посторонним")
            ;

            $table ->boolean("password")
                ->default(false)
				->comment("Игра запаролена")
            ;
			
			$table -> unsignedInteger('turn')
				->comment("номер хода")
			;
			// привязка к id статуса игры из таблицы game_statuse
			$table->foreign("status_id")->references("id")->on("game_statuses");

			// привязка к id автора игры из таблицы users
			$table->foreign("author_id")->references("id")->on("users");
			
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
