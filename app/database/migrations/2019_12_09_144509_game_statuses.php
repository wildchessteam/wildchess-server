<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class GameStatuses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game_statuses', function (Blueprint $table) {
			// id статуса партий (поле автоинкремент)
			$table -> tinyIncrements('id');
			// этот метод создаст поля created_at и updated_at
			$table->timestamps();
            $table->char('code', 32)
				->unique()
				->charset('utf8')
				->comment('Символьный код статуса партии.')
            ;
            $table->string('name', 255)
				->unique()
				->charset('utf8')
				->comment('Название статуса партии.')
            ;
        });

		DB::table('game_statuses')->insert([
			["code"=>"NEW", "name"=>"Новая"],
			["code"=>"PREPARING", "name"=>"Расстановка фигур"],
			["code"=>"PROCESS", "name"=>"Идет"],
			["code"=>"HOLD", "name"=>"Отложена"],
			["code"=>"DONE", "name"=>"Завершена"],
			["code"=>"CANSELED", "name"=>"Отменена"]
        ]);        
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
