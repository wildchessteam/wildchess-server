
<?php
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PointReasons extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('point_reasons', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->timestamps();

            $table->char('code', 16)
				->unique()
				->comment('Код причины')
            ;
            
            $table->string('name',128)
				->comment('Название причины')
            ;
            $table->mediumInteger('points')
				->comment('Количество начисленных/списанных по этой причине баллов')
            ;
            
            $table->index('code');
        });
        
      DB::table('point_reasons')->insert([
			["code"=>"VISIT", "name"=>"Посещение",'points'=>1],
			["code"=>"NOVISIT", "name"=>"Отсутствие",'points'=>-1],
			["code"=>"TASK_CREATING1", "name"=>"Создание задачи на 1 ход",'points'=>100],
			["code"=>"TASK_CREATING2", "name"=>"Создание задачи на 2 хода",'points'=>200],
			["code"=>"TASK_CREATING3", "name"=>"Создание задачи на 3 хода",'points'=>400],
			["code"=>"TASK_DECISION1", "name"=>"Решение задачи на 1 ход",'points'=>10],
			["code"=>"TASK_DECISION2", "name"=>"Решение задачи на 2 хода",'points'=>20],
			["code"=>"TASK_DECISION3", "name"=>"Решение задачи на 3 хода",'points'=>40],
			["code"=>"GAME_WIN", "name"=>"Победа в партии",'points'=>20]
        ]);
        
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
