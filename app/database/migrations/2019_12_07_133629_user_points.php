<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UserPoints extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_points', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            
            $table->integer('user_id')
				->unsigned()
				->comment('Пользователь получивший начисление или списание')
			;
            $table->integer('reason_id')
				->unsigned()
				->comment('Причина начисления/списания')
			;
            $table->mediumInteger('points')
				->comment('Количество начисленных/списанных баллов')
            ;
            $table->string('comment')->charset('utf8')
				->nullable()
				->comment('Дополнительная информация о списании/начислении')
			;
            $table->index('user_id');
            $table->index('reason_id');
            
            $table->foreign("user_id")->references("id")->on("users");
            $table->foreign("reason_id")->references("id")->on("point_reasons");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
