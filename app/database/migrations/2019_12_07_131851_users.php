<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Users extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->char('login', 48)
				->unique()
				->comment('Логин пользователя')
            ;
            $table->char('email',128)
				->unique()
				->comment('email пользователя')
            ;
            $table->char('password',40)
				->comment('Хэш пароля пользователя')
            ;
            $table->char('register_code',12)
				->comment('Код регистрации')
            ;
            $table->timestamp('registered_at')
				->nullable()
				->comment('Дата подтверждения кода регистрации')
            ;
            $table->integer('current_rating')
				->comment('Текущий рейтинг пользователя')
            ;
            
            $table->index('email');
            $table->index('login');
        });

		DB::table('users')->insert([
			[
				"login"=>"admin","email"=>"inutcin@yandex.ru",  "password"=>sha1("admininutcin@yandex.runopassword"),
				"registered_at"=>date("Y-m-d H:i:s"),"created_at"=>date("Y-m-d H:i:s"),"updated_at"=>date("Y-m-d H:i:s"),
				"register_code"=>"123456789012","current_rating"=>1000
			]
		]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
