<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TopRating extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      // Создаем таблицу top_rating
      Schema::create('top_rating', function (Blueprint $table){
        $table -> bigIncrements('id');
        
        $table -> integer('user_id')
        -> unsigned();
        
        $table -> integer('rating');
        
        $table->foreign("user_id")->references("id")->on("users");
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
