<?php
namespace wildchessteam\wildchess;

class api extends common
{

    /**
        Получаем объект необходимого API-метода
    */
    function getInstance(string $methodName){
        $className = "wildchessteam\\wildchess\\apimethod\\".$methodName;
        if(!class_exists($className))
            return $this->addError("Api method ".htmlspecialchars($methodName)." is not exists.");
        return new $className();
    }

}
