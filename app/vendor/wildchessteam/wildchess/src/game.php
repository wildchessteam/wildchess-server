<?php
namespace wildchessteam\wildchess;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class game extends common
{
    private $filter = [];
    
    public function setStatus($status)
    {
        $this->filter["games.status_id"] = $status;
        return $this;
    }
    
    public function fetchAll()
    {
		// Создаём запрос к таблицы games
		$this->dbResult = DB::table('games');
        
        // Применяем фильтры
        foreach($this->filter as $key=>$value)
            if(is_array($value))
                $this->dbResult->whereIn($key,  $value);
            else
                $this->dbResult->where($key,  '=', $value);
            
        $this->dbResult->join('users as user','games.author_id','=','user.id');
        $this->dbResult->join('game_statuses as status','games.status_id','=','status.id');
        
        $this->dbResult->select(
            'games.id as id',
            'games.turn as turn',
            'games.viewers as viewers',
            'games.password as password',
            'games.name as name',
            'user.id as user_id',
            'user.login as user_login',
            'status.id as status_id',
            'status.name as status_name'
        );

        $this->dbResult->limit($this->onPage);
        $this->dbResult->offset(($this->page-1)*$this->onPage);
    }
    
    public function getAll()
    {
        $rows = parent::getAll();
        $result = [];
        foreach($rows as $row){
            $result[] = [
				"id"=>$row->id,
				"author"=> [
					"id"=>$row->user_id,
					"login"=>$row->user_login
				],
				"name"=>$row->name,
                "status"=>[
                    "id"=>$row->status_id,
                    "name"=> $row->status_name
                ],
                "turn"=>$row->turn,
                "viewers"=>$row->viewers,
                "password"=>$row->password
            ];
        }
        
        return $result;
    }
}
