<?php
namespace wildchessteam\wildchess;

use Illuminate\Support\Facades\Validator;

class user extends common
{
    function add(string $email)
    {
        $validator = Validator::make(
            ["email"=>$email],["email"=>"required|email:rfc,dns"]
        );
        if($validator->fails()){
            return $this->addError('Некорректный email');
        }
        return $this->addError('Ошибка добавления пользователя');
    }
}
