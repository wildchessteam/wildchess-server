<?php
namespace wildchessteam\wildchess;

class common
{
    private $arErrors = []; //!< Массив последних ошибок
    protected $page = 1;
    protected $onPage = 10;
    protected $dbResult = null;
    protected $rows = [];
    
    function __construct()
    {
        return true;
    }

    /**
        Добавление ошибки в список
        @param $error - текст ошибки или массив с текстами ошибок
        @param $level - уровень ошибки 
            - emergency
            - alert
            - critical
            - error
            - warning
            - notice
            - info
            - debug
     */
    function addError($error, string $level='error'){
        if(is_array($error) && $error){  
            foreach($error as $err)$this->arErrors[] = $err;
        }
        elseif($error){
            $this->arErrors[] = $error;
        }
        \Log::$level($error);
        return false;
    }
    
    function getErrors(){
        return $this->arErrors;
    }
    
    public function setPage(int $page=1)
    {
        $this->page = $page;
        return $this;
    }
    
    public function setOnPage(int $onPage=10)
    {
        $this->onPage = $onPage;
        return $this;
    }

    public function getAll()
    {
        return $this->dbResult->get();
    }
    
    public function getCount()
    {
        return $this->dbResult->count();
    }
    
    
}
