<?php
namespace wildchessteam\wildchess\apimethod;
use wildchessteam\wildchess as wildchess;

/**
    Метод пинга пользователя
*/
class ping extends apimethod
{
    function execute(array $jsonParams=[]){
        $this->data = [];
        return parent::execute($jsonParams);
    }
}
