<?php
namespace wildchessteam\wildchess\apimethod;

/**
    Метод получения hallChat

*/
class hallChat extends apimethod {
    function execute(array $jsonParams=[]){
        $this->data = $this -> getFakeData();
        return parent::execute($jsonParams);
    }


 private function getFakeData()  {
		return [
			"messages"=>[
            [
                "timestamp" => 1570914245,
                "user"=>[
                    "id"=>2,
                    "login"=>"LeLoL",
                    "avatar"=>"https://st.depositphotos.com/1815767/1398/v/450/depositphotos_13981819-stock-illustration-chess-pieces-sketch.jpg"
                ],
                "text"=>"Всем привет!"
            ],
            [
                "timestamp"=>1570924245,
                "user"=>[
                    "id"=>3,
                    "login"=>"_LoLtik",
                    "avatar"=>"https://banner2.kisspng.com/20180710/cke/kisspng-computer-icons-pawn-chess-clip-art-5b4440129a39d0.6314252615311995066317.jpg"
                ],
                "text"=>"Здарова."
             ],
             [
                "timestamp"=>1570924245,
                "user"=>[
                    "id"=>4,
                    "login"=>"LeLoL",
                    "avatar"=>"https://st.depositphotos.com/1815767/1398/v/450/depositphotos_13981819-stock-illustration-chess-pieces-sketch.jpg"
                ],
              
                "text"=>"Го в партию 'Заруба'."
             ],
             [
                "timestamp"=>1570924245,
                "user"=>[
                    "id"=>5,
                    "login"=>"chesser",
                    "avatar"=>"https://st.depositphotos.com/1815767/1398/v/450/depositphotos_13981819-stock-illustration-chess-pieces-sketch.jpg"
                ],
                "text"=>"Не, я пойду в третью комнату."
             ]
		]
		];
	}
}

   
