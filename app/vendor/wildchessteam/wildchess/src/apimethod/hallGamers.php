<?php
namespace wildchessteam\wildchess\apimethod;

/**
    Метод получения hallGamers

*/
class hallGamers extends apimethod {
    function execute(array $jsonParams=[]){
        $this->data = $this -> getFakeData();
        return parent::execute($jsonParams);
    }


 private function getFakeData()  {
		return [
			"gamers" => 
			[
				[
					"id" => 1,
					"login" => "chesser",
					"avatar" => "http://newwaysys.com/wp-content/uploads/sk/thumb-sketch-of-a-knight-chess-piece-vectors.jpg"
				],
				[
					"id" => 2,
					"login" => "ProChess_",
					"avatar" =>"https://st.depositphotos.com/1815767/1398/v/450/depositphotos_13981819-stock-illustration-chess-pieces-sketch.jpg"
				],
				[
					"id" => 3,
					"login" => "LeLoL",
					"avatar" =>"https://banner2.kisspng.com/20180710/cke/kisspng-computer-icons-pawn-chess-clip-art-5b4440129a39d0.6314252615311995066317.jpg"
				],
				[
					"id" => 4,
					"login" => "_LoLtik",
					"avatar" => "http://newwaysys.com/wp-content/uploads/sk/thumb-sketch-of-a-knight-chess-piece-vectors.jpg"
				]		
		
			]
		];

	}
}

   
