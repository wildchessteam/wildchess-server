<?php
namespace wildchessteam\wildchess\apimethod;
use wildchessteam\wildchess as wildchess;

/**
    Метод регистрации пользователя

*/
class register extends apimethod
{
    function execute(array $jsonParams=[]){

        $user = new wildchess\user;
        if(!$user->add($jsonParams["email"])){
            $this->addError($user->getErrors());
            return parent::execute($jsonParams);
        }
        $this->data = [];
        return parent::execute($jsonParams);
    }
}
