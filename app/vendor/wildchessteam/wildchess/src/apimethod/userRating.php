<?php
namespace wildchessteam\wildchess\apimethod;

use Illuminate\Support\Facades\DB;

/**
    Метод получения рейтинга игрока

*/
class userRating extends apimethod
{
    function execute(array $jsonParams=[]){
        //$this->data = $this ->getFakeData();
        $this->data = $this ->getRows(session('user_id', 1));
        return parent::execute($jsonParams);
    }
    
    private function getRows(int $userId, int $page=1, int $onPage = 10){
        // Создаём запрос к таблице user_points
        $dbResult = DB::table('user_points')
            // Условие выборки по userId
            ->where("user_id", '=', $userId)
            // Привязываем к каждой строчке результата значение справочника point_reasons
            ->join('point_reasons as reasons','user_points.reason_id','=','reasons.id')
            // Указываем, что выбирать надо 3 столбца
            ->select(
                'user_points.created_at as timestamp',
                'reasons.name as reason',
                'user_points.points as rating'
            )
            // Порядок вывода по дате по убыванию
            ->orderBy('timestamp', 'desc')
            // Выводить только $onPage результатов
            ->limit($onPage)
            // Выводить результаты с указанного
            ->offset(($page-1)*$onPage)
            ;
            
        return [
            "history"		=>	$dbResult->get(),
            "total"			=>	$dbResult->count(),
            "page"			=>	$page,
            "on_page"	=>	$onPage
        ];
	}
    
    private function getFakeData(){
		return [
			"history" => [
				["timestamp"=>1570914245,
                "reason"=>"Активное участие",
                "rating"=>"+1"
                ],
                ["timestamp"=>1570814245,
                "reason"=>"Активное участие",
                "rating"=>"+1"
                ],
                ["timestamp"=>1570614245,
                "reason"=>"Решение задачи",
                "rating"=>"+5"
                ],
                ["timestamp"=>1570814245,
                "reason"=>"Победа в партии",
                "rating"=>"+30"
                ],
                ["timestamp"=>1570814245,
                "reason"=>"Поражение в партии",
                "rating"=>"-10"
                ],
                ["timestamp"=>1570814245,
                "reason"=>"Решение задачи",
                "rating"=>"+1"
                ],
                ["timestamp"=>1570814245,
                "reason"=>"Решение задачи",
                "rating"=>"+1"
                ],
                ["timestamp"=>1570814245,
                "reason"=>"Решение задачи",
                "rating"=>"+1"
                ],
                ["timestamp"=>1570814245,
                "reason"=>"Решение задачи",
                "rating"=>"+1"
                ],
                ["timestamp"=>1570814245,
                "reason"=>"Решение задачи",
                "rating"=>"+1"
                ],
                
                
			]
		
		
		];

	}
}
