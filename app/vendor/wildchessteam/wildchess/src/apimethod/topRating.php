<?php
namespace wildchessteam\wildchess\apimethod;

use Illuminate\Support\Facades\DB;

/**
    Метод получения top рейтинга

*/
class topRating extends apimethod
{
    function execute(array $jsonParams=[]){
        //$this->data = $this ->getFakeData();
        $this->data = $this ->getRows(session('user_id', 1));
        return parent::execute($jsonParams);
    }
    
    private function getRows(int $userId, int $page=1, int $onPage = 30){
        // Создаём запрос к таблице users
        $dbResult = DB::table('users')
            // Указываем, что выбирать надо 3 столбца
            ->select(
                'users.id as id',
                'users.login as login',
                'users.current_rating as rating')
            // Порядок вывода по дате по убыванию
            ->orderBy('rating', 'desc')
            // Выводить только $onPage результатов
            ->limit($onPage)
            // Выводить результаты с указанного
            ->offset(($page-1)*$onPage)
            ;
            
        $rows = $dbResult->get();
        
        $result = [];
        foreach($rows as $num=>$row){
            $result[] = [
                "user"=>[
                    "id"=>$row->id,
                    "login"=>$row->login,
                    "avatar"=>"https://st.depositphotos.com/1815767/1398/v/450/depositphotos_13981819-stock-illustration-chess-pieces-sketch.jpg"
                ],
                "rating"=>$row->rating
            ];
        }
        
        return [
            "top"				=>	$result,
            "total"			=>	$dbResult->count(),
            "page"			=>	$page,
            "on_page"	=>	$onPage
        ];
	}
}

