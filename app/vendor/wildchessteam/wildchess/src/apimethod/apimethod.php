<?php
namespace wildchessteam\wildchess\apimethod;


class apimethod extends \wildchessteam\wildchess\common
{
    protected $data = [];
    private $t0;
    private $t1;
    
    function __construct(){
        $this->t0 = microtime(true);
    }
    
    function execute(array $jsonParams = []){
        return [
            "execution_time_ms"=>(microtime(true) - $this->t0)*1000,
            "errors_count"=>count($this->getErrors()),
            "errors" => $this->getErrors(),
            "result"=>$this->data
        ];
    }
}

