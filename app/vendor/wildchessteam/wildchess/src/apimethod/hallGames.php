<?php
namespace wildchessteam\wildchess\apimethod;

use wildchessteam\wildchess as wildchess;

/**
    Метод получения hallGames

*/
class hallGames extends apimethod {
	
	private $showStatuses = [1,2,3];
	
    function execute(array $jsonParams=[]){
        //$this->data = $this -> getFakeData();
        $this->data = $this -> getRows(session('user_id', 1));
        return parent::execute($jsonParams);
    }

	private function getRows(int $userId, int $page=1, int $onPage = 10){
        
        $game = new wildchess\game;
        
        $game
            ->setStatus($this->showStatuses)
            ->setPage($page)
            ->setOnPage($onPage)
            ->fetchAll()
        ;
        
        return [
            "games"		=>	$game->getAll(),
            "total"		=>	$game->getCount(),
            "page"		=>	$page,
            "on_page"	=>	$onPage
        ];
	}
    
    private function getFakeData()  {
		return [
		"games"=> [
			[
				"id"=>1,
				"author"=> [
					"id"=>1,
					"login"=>"chesser"
				],
				"name"=>"Заруба",
                "status"=>[
                    "id"=>1,
                    "name"=> "Новая"
                ],
                "turn"=>0,
                "viewers"=>true,
                "password"=>true
			],
			[
				"id"=>2,
				"author"=>[
					"id"=>2,
					"login"=>"chesser1"
				],
				"name"=>"Посторонних не ждем",
                "status"=>[
                    "id"=>2,
                    "name"=> "Расстановка фигур"
                ],
                "turn"=> 0,
                "viewers"=> true,
                "password"=>true
			],
			[
				"id"=>3,
				"author"=>[
					"id"=>3,
					"login"=>"chesser2"
				],
				"name"=>"Разыгрываем только итальянским дебютом! Кто нарушает, тому БАН!",
                "status"=>[
                    "id"=>3,
                    "name"=> "Идёт"
                ],
                "turn"=>3,
                "viewers"=>true,
                "password"=>false
			],
		]
	];
	}    
}

   
