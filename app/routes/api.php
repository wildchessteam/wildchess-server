<?php

use Illuminate\Http\Request;
use wildchessteam\wildchess as wildchess;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/

Route::post('',function(Request $request){
    // Имя класса обработчика метода API
    $api = new wildchess\api;
    
    $method = $api->getInstance($request->post('method'));
    
    // Обрабатываем ошибки входных данных
    if($errors = $api->getErrors())
        return response()
            ->json(["errors_count"=>count($errors), "errors"=>$errors], 500)
            ->header("Access-Control-Allow-Origin", "*");
        
    // Получаем ответ метода
    $methodResult = $method->execute(
        json_decode($request->post('params'), true)
        ?
        json_decode($request->post('params'), true):
        []
    );

    $responseCode = 200;
    if($methodResult["errors_count"]>0)$responseCode = 500;

    return response()
        ->json($methodResult,$responseCode)
        ->header("Access-Control-Allow-Origin", "*");
});
